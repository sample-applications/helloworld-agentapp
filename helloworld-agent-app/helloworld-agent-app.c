/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gtk/gtk.h>

#include "helloworld-agent-app.h"
#include "helloworld-agent-window.h"
#include "org.apertis.HelloWorld.AgentApp.Agent.TickBoard.h"

/* Agent Bus ID */
#define HLW_AGENT_ID "org.apertis.HelloWorld.AgentApp.Agent"

struct _HlwAgentApp
{
  GtkApplication parent;
};

G_DEFINE_TYPE (HlwAgentApp, hlw_agent_app, GTK_TYPE_APPLICATION);

static void
hlw_agent_app_activate (GApplication *app)
{
  HlwAgentAppWindow *win = NULL;
  win = hlw_agent_app_window_new (HLW_AGENT_APP (app));
  gtk_window_present (GTK_WINDOW (win));
}

static void
hlw_agent_app_class_init (HlwAgentAppClass *klass)
{
  G_APPLICATION_CLASS (klass)->activate = hlw_agent_app_activate;
}

static void
hlw_agent_app_init (HlwAgentApp *self)
{
}

HlwAgentApp *
hlw_agent_app_new (void)
{
  return g_object_new (HLW_AGENT_TYPE_APP,
                       "application-id", "org.apertis.HelloWorld.AgentApp",
                       NULL);
}
