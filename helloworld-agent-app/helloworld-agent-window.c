/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "helloworld-agent-app.h"
#include "org.apertis.HelloWorld.AgentApp.Agent.TickBoard.h"
#include "helloworld-agent-window.h"

/* Agent Bus ID */
#define HLW_AGENT_ID "org.apertis.HelloWorld.AgentApp.Agent"

#define WIN_WIDTH  480
#define WIN_HEIGHT 800

struct _HlwAgentAppWindow
{
  GtkApplicationWindow parent;
  GtkWidget *window;            /* owned */
  GtkWidget *button;            /* owned */
  GtkWidget *button_box;        /* owned */
  HlwDBusTickBoard *hlw_tick_board;   /* owned */
  guint hlw_tick_board_bus_id;
};

G_DEFINE_TYPE(HlwAgentAppWindow, hlw_agent_app_window, GTK_TYPE_APPLICATION_WINDOW);

static void
on_current_tick_updated (GObject *object,
                         GParamSpec *psec,
                         gpointer user_data)
{
  int tick = 0;
  g_autofree gchar *tick_text = NULL;
  HlwAgentAppWindow *self = HLW_AGENT_APP_WINDOW (user_data);

  /* Draw tick value on the screen */
  g_object_get (self->hlw_tick_board, "current-tick", &tick, NULL);
  tick_text = g_strdup_printf ("Tick Value : %d", tick);
  gtk_button_set_label(GTK_BUTTON(self->button), tick_text);
}

static void
on_toggle_tick_callback (GObject *source_object,
                         GAsyncResult *result,
                         gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  HlwDBusTickBoard *tick_board = HLW_DBUS_TICK_BOARD (source_object);

  if (!hlw_dbus_tick_board_call_toggle_tick_finish (tick_board, result, &error))
    {
      g_warning ("Failed to finish toggle_tick request (reason: %s)",
        error->message);
    }
}

static void
button_pressed_cb (GtkWidget *widget,
                   gpointer user_data)
{
  HlwAgentAppWindow *self = HLW_AGENT_APP_WINDOW (user_data);

  if (self->hlw_tick_board == NULL)
    {
      g_message ("Agent proxy isn't ready.");

      return;
    }

  g_message ("Clicked Toggle button");

  hlw_dbus_tick_board_call_toggle_tick (self->hlw_tick_board,
                                        NULL,
                                        on_toggle_tick_callback,
                                        self);
}

static void
hlw_dbus_tick_board_ready_callback (GObject *source_object,
                                    GAsyncResult *res,
                                    gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  HlwAgentAppWindow *self = HLW_AGENT_APP_WINDOW (user_data);

  self->hlw_tick_board = hlw_dbus_tick_board_proxy_new_finish (res, &error);

  g_signal_connect (self->hlw_tick_board,
      "notify::current-tick", G_CALLBACK (on_current_tick_updated),
      self);

  g_message ("Created TickBoard Proxy (%p).", self->hlw_tick_board);
}

static void
bus_name_appeared (GDBusConnection *connection,
                   const gchar *name,
                   const gchar *name_owner,
                   gpointer user_data)
{
  HlwAgentAppWindow *self = HLW_AGENT_APP_WINDOW (user_data);

  g_message ("Bus name %s now owned by %s", name, name_owner);

  if (g_strcmp0 (name, "org.apertis.HelloWorld.AgentApp.Agent") == 0)
    {
      /* The TickBoard will be created asynchronously. */
      hlw_dbus_tick_board_proxy_new (connection,
        G_DBUS_PROXY_FLAGS_NONE,
        "org.apertis.HelloWorld.AgentApp.Agent",
        "/org/apertis/HelloWorldAgentApp/Agent/TickBoard",
        NULL,
        hlw_dbus_tick_board_ready_callback, self);
    }
}

static void
bus_name_vanished (GDBusConnection *connection,
                   const gchar *name,
                   gpointer user_data)
{
  HlwAgentAppWindow *self = HLW_AGENT_APP_WINDOW (user_data);

  if (g_strcmp0 (name, "org.apertis.HelloWorld.AgentApp.Agent") == 0)
    {
      g_message ("Destroying TickBoard proxy (%p).", self->hlw_tick_board);
      g_clear_object (&self->hlw_tick_board);
    }
}

static void
hlw_agent_app_window_init (HlwAgentAppWindow *app)
{
  HlwAgentAppWindow *win = HLW_AGENT_APP_WINDOW(app);
  win->hlw_tick_board_bus_id = g_bus_watch_name (G_BUS_TYPE_SESSION, HLW_AGENT_ID,
		  	  	  	  	  	  	  	  	  	  	 G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
												 bus_name_appeared,
												 bus_name_vanished,
												 win, NULL);
  gtk_window_set_default_size(GTK_WINDOW(win), WIN_WIDTH, WIN_HEIGHT);

  win->button = gtk_button_new_with_label("Click!");
  g_signal_connect (win->button, "clicked", G_CALLBACK(button_pressed_cb), win);

  win->button_box = gtk_button_box_new (GTK_ORIENTATION_HORIZONTAL);
  gtk_container_add (GTK_CONTAINER (win->button_box), win->button);
  gtk_container_add (GTK_CONTAINER(win), GTK_WIDGET(win->button_box));

  gtk_widget_show_all(GTK_WIDGET(win));
}

static void
hlw_agent_app_window_class_init (HlwAgentAppWindowClass *class)
{
}

HlwAgentAppWindow *
hlw_agent_app_window_new (HlwAgentApp *app)
{
  return g_object_new (HLW_AGENT_APP_WINDOW_TYPE, "application", app, NULL);
}
