/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __HELLOWORLD_TICK_BOARD_H__
#define __HELLOWORLD_TICK_BOARD_H__

#include <glib-object.h>
#include <gio/gio.h>

#include "org.apertis.HelloWorld.AgentApp.Agent.TickBoard.h"

G_BEGIN_DECLS

#define HLW_TYPE_TICK_BOARD (hlw_tick_board_get_type ())
G_DECLARE_FINAL_TYPE (HlwTickBoard, hlw_tick_board, HLW, TICK_BOARD, HlwDBusTickBoardSkeleton)
HlwTickBoard *hlw_tick_board_new (void);

G_END_DECLS

#endif /* __HELLOWORLD_TICK_BOARD_H__*/
