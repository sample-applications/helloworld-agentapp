/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <gio/gio.h>

#include "helloworld-agent.h"

#define HLW_AGENT_ID "org.apertis.HelloWorld.AgentApp.Agent"

int
main (int argc, char **argv)
{
  g_autoptr (GApplication) app = NULL;

  app = G_APPLICATION (g_object_new (HLW_TYPE_AGENT,
      "application-id", HLW_AGENT_ID,
      "flags", G_APPLICATION_IS_SERVICE,
      NULL));

  return g_application_run (app, argc, argv);
}
